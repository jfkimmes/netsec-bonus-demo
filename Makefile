.PHONY: all build run

# === Use one of the three following lines to chose the container tech of your choice ===
CONTAINER = flatpak-spawn --host podman
#CONTAINER = podman
#CONTAINER = docker
# ===

all: run

demo-container/netsec.war: ./demo-app/src/main/java/eu/jfkimmes/netsec/$(.*\.java)
	cd demo-app; ./mvnw clean install
	cp ./demo-app/target/netsec-0.0.1-SNAPSHOT.war ./demo-container/netsec.war

run: build
	$(CONTAINER) run -p 8080:8080 --name netsec-demo --rm localhost/ubuntu:netsec

build: demo-container/netsec.war
	cd demo-container; $(CONTAINER) build --tag ubuntu:netsec -f ./Dockerfile

clean:
	rm -rf ./demo-app/target
	rm -rf ./demo-container/netsec.war

demo: build-demo
	$(CONTAINER) run -p 8080:8080 --name netsec-demo --rm localhost/ubuntu:netsec

build-demo:
	cd demo-container; $(CONTAINER) build --tag ubuntu:netsec -f ./Dockerfile-Demo

