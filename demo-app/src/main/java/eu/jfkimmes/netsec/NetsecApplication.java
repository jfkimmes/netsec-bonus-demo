package eu.jfkimmes.netsec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetsecApplication {
	public static void main(String[] args) {
		SpringApplication.run(NetsecApplication.class, args);
	}
}

